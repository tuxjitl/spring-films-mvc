$('#selActors').on('change', function () {

    let selectedValue = $(this).find(':selected').val();
    let inputActor = document.getElementById('actorlist')
    let listActors = inputActor.value;

    if (listActors.length > 0) {

        listActors += ', ' + selectedValue;
    } else {
        listActors += selectedValue;
    }

    document.getElementById('actorlist').value = listActors;

});
