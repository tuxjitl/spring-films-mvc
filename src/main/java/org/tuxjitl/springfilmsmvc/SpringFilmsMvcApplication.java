package org.tuxjitl.springfilmsmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.tuxjitl.springfilmsmvc.model.Film;
import org.tuxjitl.springfilmsmvc.services.FilmService;
import org.tuxjitl.springfilmsmvc.services.FilmServiceImpl;

import java.time.LocalDate;

@SpringBootApplication
public class SpringFilmsMvcApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx = SpringApplication.run(SpringFilmsMvcApplication.class, args);

        FilmService filmService = ctx.getBean("filmServiceImpl", FilmServiceImpl.class);

        Film newFilm = new Film();
        newFilm.setTitle("Lord of the Rings 3");
        newFilm.setReleaseDate(LocalDate.of(2021, 1, 8));
        newFilm.setUrlPoster("https://media.s-bol.com/7Nv88oRROjG/550x777.jpg");

        filmService.registerFilm(newFilm);

        System.out.println(filmService.retrieveFilmById(1L));

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

}
