package org.tuxjitl.springfilmsmvc.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.tuxjitl.springfilmsmvc.model.Actor;
import org.tuxjitl.springfilmsmvc.model.Film;
import org.tuxjitl.springfilmsmvc.model.Role;
import org.tuxjitl.springfilmsmvc.model.User;
import org.tuxjitl.springfilmsmvc.services.ActorService;
import org.tuxjitl.springfilmsmvc.services.FilmService;
import org.tuxjitl.springfilmsmvc.services.RoleService;
import org.tuxjitl.springfilmsmvc.services.UserService;

import java.time.LocalDate;

//CommandLineRunner ==> spring-boot specific way
//@Component: this becomes a bean, spring detects it
// and registers it as bean in the context and executes it (commandline runner => run)
@Component
@Profile("mem")
public class FilmData implements CommandLineRunner {

    private final FilmService filmService;
    private final ActorService actorService;
    private final RoleService roleService;
    private final UserService userService;

    //specifying the interface as parameter type ==> not dependant of underlying type


    public FilmData(FilmService filmService, ActorService actorService, RoleService roleService, UserService userService) {

        this.filmService = filmService;
        this.actorService = actorService;
        this.roleService = roleService;
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {

        // Create POJO's for Actors

        Actor act1 = new Actor(1L,"an","aalst","https://www.companyfolders.com/blog/media/2017/07/the-silence-of-the-lambs.jpg" );
        Actor act2 = new Actor(2L,"bert","beringen","http://www.cheatsheet.com/wp-content/uploads/2016/04/GettyImages-516772830.jpg" );
        Actor act3 = new Actor(3L,"charlie","caen","https://ae01.alicdn.com/kf/HTB18scIHVXXXXagXVXXq6xXFXXXb/Channing-Tatum-Movie-Actor-Star-Fabric-Poster-20-x-13-005.jpg_Q90.jpg_.webp" );
        Actor act4 = new Actor(4L,"Dirk","dustel","https://images-na.ssl-images-amazon.com/images/I/61Ehy-ISW-L._AC_SX450_.jpg" );
        Actor act5= new Actor(5L,"Eric","embers","https://ih1.redbubble.net/image.2217846194.5171/poster,504x498,f8f8f8-pad,600x600,f8f8f8.u1.jpg" );
        Actor act6 = new Actor(6L,"Freddy","fyou","https://ih1.redbubble.net/image.759821027.0792/poster,504x498,f8f8f8-pad,600x600,f8f8f8.u3.jpg" );
        Actor act7 = new Actor(7L,"Guy","gestel","https://ih1.redbubble.net/image.1164929522.1072/poster,504x498,f8f8f8-pad,600x600,f8f8f8.u1.jpg" );
        Actor act8 = new Actor(8L,"Henri","homer","https://ih1.redbubble.net/image.1095358853.5324/poster,504x498,f8f8f8-pad,600x600,f8f8f8.jpg" );
        Actor act9 = new Actor(19,"Isa","isegem", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRbmzZylur9PoDBWGHb3Xtw7YI1XKYskBsElQ&usqp=CAU");
        Actor act10 = new Actor(10L,"Jeannette","janeiro","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwGd7KQVN_80Fuy3LEQ6Bvcx7o_c7Zuofl7A&usqp=CAU" );

        actorService.registerActor(act1);
        actorService.registerActor(act2);
        actorService.registerActor(act3);
        actorService.registerActor(act4);
        actorService.registerActor(act5);
        actorService.registerActor(act6);
        actorService.registerActor(act7);
        actorService.registerActor(act8);
        actorService.registerActor(act9);
        actorService.registerActor(act10);

        //Creating POJO's to be used as db data
        Film newFilm1 = new Film();
        newFilm1.setTitle("Lord of the Rings");
        newFilm1.setReleaseDate(LocalDate.of(2012, 5, 8));
        newFilm1.setUrlPoster("https://static.posters.cz/image/750/posters/lord-of-the-rings-trilogy-i11353.jpg");
        newFilm1.addActor(act1);
        newFilm1.addActor(act2);
        filmService.registerFilm(newFilm1);

        Film newFilm2 = new Film();
        newFilm2.setTitle("Star Wars Attack of the clones");
        newFilm2.setReleaseDate(LocalDate.of(2002, 5, 8));
        newFilm2.setUrlPoster("https://i.frog.ink/j5Eozwnv/swnmr94254_600.jpg?v=1566138099.947");
        newFilm2.addActor(act4);
        newFilm2.addActor(act2);
        filmService.registerFilm(newFilm2);

        Film newFilm3 = new Film();
        newFilm3.setTitle("Star Wars 3 Revenge of the sith");
        newFilm3.setReleaseDate(LocalDate.of(2013, 4, 18));
        newFilm3.setUrlPoster("https://i.pinimg.com/originals/be/8c/31/be8c31419d582cecd51a367afdac3cdf.png");
        newFilm3.addActor(act7);
        newFilm3.addActor(act3);
        filmService.registerFilm(newFilm3);

        Film newFilm4 = new Film();
        newFilm4.setTitle("Jaws");
        newFilm4.setReleaseDate(LocalDate.of(1980, 5, 24));
        newFilm4.setUrlPoster("https://images-na.ssl-images-amazon.com/images/I/51aUE4WyciL._AC_SY679_.jpg");
        newFilm4.addActor(act10);
        newFilm4.addActor(act8);
        filmService.registerFilm(newFilm4);

        Film newFilm5 = new Film();
        newFilm5.setTitle("Jaws2");
        newFilm5.setReleaseDate(LocalDate.of(1982, 5, 14));
        newFilm5.setUrlPoster("https://images-na.ssl-images-amazon.com/images/I/51Oacs42TtL._AC_.jpg");
        newFilm5.addActor(act7);
        newFilm5.addActor(act8);
        filmService.registerFilm(newFilm5);

        Film newFilm6 = new Film();
        newFilm6.setTitle("Seven");
        newFilm6.setReleaseDate(LocalDate.of(1992, 5, 14));
        newFilm6.setUrlPoster("https://images-na.ssl-images-amazon.com/images/I/61wp69HpViL._AC_.jpg");
        newFilm6.addActor(act3);
        newFilm6.addActor(act8);
        filmService.registerFilm(newFilm6);

        Film newFilm7 = new Film();
        newFilm7.setTitle("Along came a spider");
        newFilm7.setReleaseDate(LocalDate.of(1990, 8, 7));
        newFilm7.setUrlPoster("https://images-na.ssl-images-amazon.com/images/I/517wM%2Bsl9DL._AC_.jpg");
        newFilm7.addActor(act7);
        newFilm7.addActor(act6);
        filmService.registerFilm(newFilm7);

        Film newFilm8 = new Film();
        newFilm8.setTitle("Battle at the river Plate");
        newFilm8.setReleaseDate(LocalDate.of(1955, 10, 29));
        newFilm8.setUrlPoster("https://images-na.ssl-images-amazon.com/images/I/51jk2BJJ9WL._AC_.jpg");
        newFilm8.addActor(act5);
        newFilm8.addActor(act7);
        filmService.registerFilm(newFilm8);

        Film newFilm9 = new Film();
        newFilm9.setTitle("The bodygouard");
        newFilm9.setReleaseDate(LocalDate.of(1992, 5, 8));
        newFilm9.setUrlPoster("https://images-na.ssl-images-amazon.com/images/I/51GLgQ203iL._AC_.jpg");
        newFilm9.addActor(act7);
        newFilm9.addActor(act4);
        filmService.registerFilm(newFilm9);

        Film newFilm10 = new Film();
        newFilm10.setTitle("Operation pettycoat");
        newFilm10.setReleaseDate(LocalDate.of(1960, 4, 21));
        newFilm10.setUrlPoster("https://picsum.photos/300");
        newFilm10.addActor(act7);
        newFilm10.addActor(act10);
        filmService.registerFilm(newFilm10);

        //create role user
        Role role = new Role();
        role.setName("USER");
        roleService.registerRole(role);

        Role roleAdmin = new Role();
        roleAdmin.setName("ADMIN");
        roleService.registerRole(roleAdmin);


        //create login user
        User jc = new User();
        jc.setUsername("jc");
        jc.setPassword("jc");
        jc.setAvatarUrl("https://picsum.photos/300");
        jc.setFullName("Leurs Jean-Claude");
        jc.setCountry("Belgium");
        jc.setFaveMovieQuote("I'll be back");
        userService.register(jc);


        //create login user with admin role
        User jcl = new User();
        jcl.setUsername("jcl");
        jcl.setPassword("jcl");
        jcl.setAvatarUrl("https://www.geekish.nl/wp-content/uploads/2020/09/Avatar-The-Last-Airbender-Netflix-1024x576.png");
        jcl.setFaveMovieQuote("Supposition is the mother of all fuck-ups");
        jcl.addRole(roleAdmin);
        userService.register(jcl);


    }
}
