package org.tuxjitl.springfilmsmvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tuxjitl.springfilmsmvc.model.Actor;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActorRepository extends JpaRepository<Actor,Long> {

    Optional<Actor> findActorByFirstNameAndLastName(String fname, String lName);
    List<Actor> findActorsByLastName(String lName);

}
