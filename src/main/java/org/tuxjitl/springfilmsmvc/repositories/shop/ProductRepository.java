package org.tuxjitl.springfilmsmvc.repositories.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tuxjitl.springfilmsmvc.model.shop.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
