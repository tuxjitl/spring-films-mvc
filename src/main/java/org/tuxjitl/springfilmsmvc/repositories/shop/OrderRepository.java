package org.tuxjitl.springfilmsmvc.repositories.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tuxjitl.springfilmsmvc.model.shop.Order;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

    List<Order> findOrdersByUser_Username(String username);
    Optional<Order> findOrderByIdAndUser_Username(Long id, String username);

}
