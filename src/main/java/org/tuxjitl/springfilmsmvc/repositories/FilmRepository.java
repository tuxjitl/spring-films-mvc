package org.tuxjitl.springfilmsmvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tuxjitl.springfilmsmvc.model.Film;

import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository<Film,Long> {
    List<Film> findByTitleContainingIgnoreCase(String title);
}
