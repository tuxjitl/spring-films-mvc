package org.tuxjitl.springfilmsmvc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tuxjitl.springfilmsmvc.model.Actor;
import org.tuxjitl.springfilmsmvc.model.Film;
import org.tuxjitl.springfilmsmvc.repositories.FilmRepository;

import java.util.*;

@Service
public class FilmServiceImpl implements FilmService{

    FilmRepository filmRepository;
    private final ActorService actorService;

    @Autowired
    public FilmServiceImpl(FilmRepository filmRepository, ActorService actorService) {

        this.filmRepository = filmRepository;
        this.actorService = actorService;
    }

    @Override
    public Optional<Film> retrieveFilmById(Long id) {

        Optional<Film> film = null;
        film = filmRepository.findById(id);
        return film;

    }

    @Override
    public List<Film> retrieveAllFilms() {

        List<Film> films = filmRepository.findAll();
        return films;
    }

    @Override
    public void registerFilm(Film film) {
        filmRepository.save(film);
    }

    @Override
    public List<Film> searchByTitle(String title) {

//        return filmRepository.findByTitle(title);
        return filmRepository.findByTitleContainingIgnoreCase(title);
    }

    @Override
    public void addActorsToFilm(Film film,String actorList) {
        Set<Actor> actorsOnThisFilm = new HashSet<>();
        List<String> fullNames = Arrays.asList((actorList.split(",")));
        Actor actor = null;
        if (actorList.length()>0) {
            for (String name : fullNames) {
                String cleanedName = name.trim();
                String[] splitFullname = cleanedName.split(":");
                Actor toSearchActor = actorService.retrieveActorByFirstNameAndLastName(
                        splitFullname[0],splitFullname[1]
                );
                if (toSearchActor==null){
                    actor = new Actor(splitFullname[0], splitFullname[1]);

                }else{
                    actor=toSearchActor;
                }
                actorsOnThisFilm.add(actor);
                actorService.registerActor(actor);
            }
            film.setActors(actorsOnThisFilm);
        }

    }
}
