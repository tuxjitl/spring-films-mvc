package org.tuxjitl.springfilmsmvc.services;

import org.tuxjitl.springfilmsmvc.model.Actor;

import java.util.List;
import java.util.Optional;

public interface ActorService {

    Optional<Actor> retrieveActorById(Long id);
    List<Actor> retrieveAllActors();
    void registerActor(Actor actor);
    Actor retrieveActorByFirstNameAndLastName(String fName,String lName);
    List<Actor> retrieveActorByLastName(String lName);


}
