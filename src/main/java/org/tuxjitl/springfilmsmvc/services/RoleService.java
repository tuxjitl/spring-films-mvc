package org.tuxjitl.springfilmsmvc.services;

import org.tuxjitl.springfilmsmvc.model.Role;

public interface RoleService {

    void registerRole(Role role);

}
