package org.tuxjitl.springfilmsmvc.services;

import org.springframework.stereotype.Service;
import org.tuxjitl.springfilmsmvc.model.Actor;
import org.tuxjitl.springfilmsmvc.repositories.ActorRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ActorServiceImpl implements ActorService {

    private ActorRepository actorRepository;

    public ActorServiceImpl(ActorRepository actorRepository) {

        this.actorRepository = actorRepository;

    }

    @Override
    public Optional<Actor> retrieveActorById(Long id) {

        Optional<Actor> foundActor = actorRepository.findById(id);
        return foundActor;
    }

    @Override
    public List<Actor> retrieveAllActors() {

        List<Actor> actors = actorRepository.findAll();
        return actors;
    }

    @Override
    public void registerActor(Actor actor) {

        actorRepository.save(actor);

    }

    @Override
    public Actor retrieveActorByFirstNameAndLastName(String fName, String lName) {


        return actorRepository.findActorByFirstNameAndLastName(fName, lName).orElse(null);
    }

    @Override
    public List<Actor> retrieveActorByLastName(String lastName) {

//        return filmRepository.findByTitle(title);
        return actorRepository.findActorsByLastName(lastName);
    }

}
