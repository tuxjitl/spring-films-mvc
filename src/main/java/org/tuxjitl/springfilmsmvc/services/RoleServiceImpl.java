package org.tuxjitl.springfilmsmvc.services;

import org.springframework.stereotype.Service;
import org.tuxjitl.springfilmsmvc.model.Role;
import org.tuxjitl.springfilmsmvc.repositories.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {

        this.roleRepository = roleRepository;
    }

    @Override
    public void registerRole(Role role) {
        roleRepository.save(role);
    }
}
