package org.tuxjitl.springfilmsmvc.services;

import org.tuxjitl.springfilmsmvc.model.User;

public interface UserService {
    void register(User user);
    User retrieveByUsername(String username);
    void updateUser(User user);
}
