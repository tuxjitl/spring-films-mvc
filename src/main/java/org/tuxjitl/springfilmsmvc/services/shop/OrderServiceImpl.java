package org.tuxjitl.springfilmsmvc.services.shop;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.tuxjitl.springfilmsmvc.model.User;
import org.tuxjitl.springfilmsmvc.model.shop.Order;
import org.tuxjitl.springfilmsmvc.model.shop.OrderItem;
import org.tuxjitl.springfilmsmvc.repositories.shop.OrderRepository;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService{
    private final OrderRepository orderRepo;

    @Override
    public void placeOrder(List<OrderItem> orderItems, User user) {
        Order newOrder = new Order();
        for (OrderItem orderItem : orderItems) {
            newOrder.addOrderItem(orderItem);
        }
        newOrder.setUser(user);
        newOrder.setOrderDate(LocalDateTime.now());
        BigDecimal totalCost = BigDecimal.ZERO;
        for (OrderItem orderItem : orderItems) {
            BigDecimal cost = orderItem.getProduct().getCost();
            totalCost = totalCost.add(cost.multiply(BigDecimal.valueOf(orderItem.getAmount())));
        }
        newOrder.setTotalCost(totalCost);

        orderRepo.save(newOrder);
    }

    @Override
    public List<Order> retrieveOrdersByUsername(String username) {
        return orderRepo.findOrdersByUser_Username(username);
    }

    @Override
    public Order retrieveOrderByIdAndUsername(Long id, String loggedInUsername) {
        return orderRepo.findOrderByIdAndUser_Username(id, loggedInUsername)
                .orElseThrow(EntityNotFoundException::new);
    }

}
