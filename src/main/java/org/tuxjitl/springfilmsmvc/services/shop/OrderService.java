package org.tuxjitl.springfilmsmvc.services.shop;

import org.tuxjitl.springfilmsmvc.model.User;
import org.tuxjitl.springfilmsmvc.model.shop.Order;
import org.tuxjitl.springfilmsmvc.model.shop.OrderItem;

import java.util.List;

public interface OrderService {

    void placeOrder(List<OrderItem> orderItems, User user);
    List<Order> retrieveOrdersByUsername(String username);

    Order retrieveOrderByIdAndUsername(Long id, String loggedInUsername);


}
