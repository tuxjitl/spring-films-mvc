package org.tuxjitl.springfilmsmvc.services.shop;

import org.tuxjitl.springfilmsmvc.model.shop.Product;

import java.util.List;

public interface ProductService {

    void register(Product product);
    List<Product> retrieveAll();
    Product retrieveById(Long productId);


}
