package org.tuxjitl.springfilmsmvc.services.shop;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.tuxjitl.springfilmsmvc.model.shop.Product;
import org.tuxjitl.springfilmsmvc.repositories.shop.ProductRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepo;

    @Override
    public void register(Product product) {
        productRepo.save(product);
    }

    @Override
    public List<Product> retrieveAll() {
        return productRepo.findAll();
    }

    @Override
    public Product retrieveById(Long productId) {
        return productRepo.findById(productId).orElseThrow(EntityNotFoundException::new);
    }

}
