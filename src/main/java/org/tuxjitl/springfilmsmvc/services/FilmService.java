package org.tuxjitl.springfilmsmvc.services;

import org.tuxjitl.springfilmsmvc.model.Film;

import java.util.List;
import java.util.Optional;

public interface FilmService {

    Optional<Film> retrieveFilmById(Long id);
    List<Film> retrieveAllFilms();
    void registerFilm(Film film);
    List<Film> searchByTitle(String title);
    void addActorsToFilm(Film film, String actorList);

}
