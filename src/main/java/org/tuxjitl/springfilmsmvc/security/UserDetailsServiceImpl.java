package org.tuxjitl.springfilmsmvc.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tuxjitl.springfilmsmvc.model.Role;
import org.tuxjitl.springfilmsmvc.model.User;
import org.tuxjitl.springfilmsmvc.repositories.UserRepository;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException(username)
        );

        Set<GrantedAuthority> auths = new HashSet<>();
        for (Role role : user.getRoles()){
            auths.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        }

        org.springframework.security.core.userdetails.User springUser = new
                org.springframework.security.core.userdetails.User(
                        user.getUsername(), user.getPassword(),auths
        );

        return springUser;
    }

}
