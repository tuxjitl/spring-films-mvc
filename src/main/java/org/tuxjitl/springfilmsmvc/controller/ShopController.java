package org.tuxjitl.springfilmsmvc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.tuxjitl.springfilmsmvc.services.shop.ProductService;

@Controller
@RequiredArgsConstructor
public class ShopController {
    private final ProductService productService;
    @GetMapping("/shop")
    public String shopView(Model model){
        model.addAttribute("products", productService.retrieveAll());
        return "shop/shop";
    }
}

