package org.tuxjitl.springfilmsmvc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tuxjitl.springfilmsmvc.model.User;
import org.tuxjitl.springfilmsmvc.model.shop.OrderItem;
import org.tuxjitl.springfilmsmvc.model.shop.Product;
import org.tuxjitl.springfilmsmvc.services.UserService;
import org.tuxjitl.springfilmsmvc.services.shop.OrderService;
import org.tuxjitl.springfilmsmvc.services.shop.ProductService;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cart")
@RequiredArgsConstructor
public class CartController {
    private final ProductService productService;
    private final OrderService orderService;
    private final UserService userService;

    @GetMapping
    public String cartView() {
        return "shop/shopping-cart";
    }

    @PostMapping("/add/{id}")
    public String addToCart(@PathVariable("id") Long productId, HttpSession session) {
        Product foundProduct = productService.retrieveById(productId);
        OrderItem itemToAdd = new OrderItem(foundProduct, 1);
        List<OrderItem> cart;
        if (session.getAttribute("cart") == null) {
            cart = new ArrayList<>();
            cart.add(itemToAdd);
        } else {
            cart = (List<OrderItem>) session.getAttribute("cart");
            boolean existsInCart = false;
            for (OrderItem orderItem : cart) {
                if (orderItem.getProduct().getId().equals(itemToAdd.getProduct().getId())) {
                    orderItem.setAmount(orderItem.getAmount() + 1);
                    existsInCart = true;
                    break;
                }
            }
            if (!existsInCart) {
                cart.add(itemToAdd);
            }
        }
        session.setAttribute("cart", cart);

        return "redirect:/cart";
    }

    @PostMapping("/checkout")
    public String checkout(HttpSession session, Principal principal) {
        List<OrderItem> cart = (List<OrderItem>) session.getAttribute("cart");
        User loggedInUser = userService.retrieveByUsername(principal.getName());
        orderService.placeOrder(cart, loggedInUser);
        session.removeAttribute("cart");
        return "redirect:/cart?checkout";
    }

}

