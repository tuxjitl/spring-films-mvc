package org.tuxjitl.springfilmsmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.tuxjitl.springfilmsmvc.model.Actor;
import org.tuxjitl.springfilmsmvc.services.ActorService;
import org.tuxjitl.springfilmsmvc.services.FilmService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping()
public class ActorController {

    private ActorService actorService;
    private FilmService filmService;

    public ActorController(ActorService actorService, FilmService filmService) {

        this.actorService = actorService;
        this.filmService = filmService;
    }

    @GetMapping(value = "/actors", params = "lastName")
    public String searchFilms(Model model, @RequestParam(name = "lastName") String
            lastName) {

        List<Actor> searchedActors = actorService.retrieveActorByLastName(lastName);
        model.addAttribute("actors", searchedActors);
        return "actors/index";
    }

    @GetMapping({"/actors","/actors/index","/actors/index.html"})
    public String listAllActors(Model model){

        model.addAttribute("actors", actorService.retrieveAllActors());
        return "actors/index";
    }

    @GetMapping("/actors/{id}")
    public String getActorById (@PathVariable("id")  Long id, Model model) {

        Actor searchActorById = actorService.retrieveActorById(id).orElseThrow(RuntimeException::new);

        model.addAttribute("actor",searchActorById);
        return "actors/actor-details";
    }

    @GetMapping("/actors/{id}/edit")
    public String editActor(Model model, @PathVariable Long id) {

       Actor actor = actorService.retrieveActorById (id).orElseThrow(RuntimeException::new);
        model.addAttribute("actor", actor);
        return "actors/actor-new";
    }

    @GetMapping("/actors/new")
    public String newActorForm(Model model) {
//        List<Film> films = new ArrayList<>();
//        films = filmService.retrieveAllFilms();
        model.addAttribute("actor", new Actor());
//        model.addAttribute("films", films);
        return "actors/actor-new";
    }

//    @PostMapping("/actors/new")
//    public String addNewActor(@Valid Actor actor, BindingResult br,
//                             @RequestParam String filmlist) {
//
//
////        actorService.addFilmsToActor(actor, filmlist);
//
//        if (br.hasErrors()) {
//            return "actors/actor-new";
//        }
//        else {
//
//            actorService.registerActor(actor);
//            Long id = actor.getId();
//            return "actors/actor-details";
//        }
//    }

    @PostMapping("/actors/new")
    public String addNewActor(@Valid Actor actor, BindingResult br) {


//        actorService.addFilmsToActor(actor, filmlist);

        if (br.hasErrors()) {
            return "actors/actor-new";
        }
        else {

            actorService.registerActor(actor);
            Long id = actor.getId();
            return "actors/actor-details";
        }
    }


}
