package org.tuxjitl.springfilmsmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.tuxjitl.springfilmsmvc.model.Actor;
import org.tuxjitl.springfilmsmvc.model.Film;
import org.tuxjitl.springfilmsmvc.services.ActorService;
import org.tuxjitl.springfilmsmvc.services.FilmService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FilmController {

    private final FilmService filmService;
    private final ActorService actorService;
    public FilmController(FilmService filmService, ActorService actorService) {

        this.filmService = filmService;
        this.actorService = actorService;
    }

    @GetMapping({"/films", "/films/index", "/films/index.html"})
    public String listFilms(Model model) {

        model.addAttribute("films", filmService.retrieveAllFilms());

        return "films/index";
    }

    @GetMapping("/films/{id}")
    public String getFilmById(@PathVariable("id") Long id, Model model) {

        Film searchFilmById = filmService.retrieveFilmById(id).orElseThrow(RuntimeException::new);
        model.addAttribute("film", searchFilmById);
        return "films/film-details";
    }

    @GetMapping(value = "/films", params = "title")
    public String searchFilms(Model model, @RequestParam(name = "title") String
            titleSearchTerm) {

        List<Film> searchedFilms = filmService.searchByTitle(titleSearchTerm);
        model.addAttribute("films", searchedFilms);
        return "films/index";
    }

    @GetMapping("/films/new")
    public String newFilmForm(Model model) {
        List<Actor> actors = new ArrayList<>();
        actors = actorService.retrieveAllActors();
        model.addAttribute("film", new Film());
        model.addAttribute("actors", actors);
        return "films/film-new";
    }

    @PostMapping("/films/new")
    public String addNewFilm(@Valid Film film, BindingResult br,
                             @RequestParam String actorlist) {


        filmService.addActorsToFilm(film, actorlist);

        if (br.hasErrors()) {
            return "films/film-new";
        }
        else {

            filmService.registerFilm(film);
            Long id = film.getId();
            return "films/film-details";
        }
    }

    @GetMapping("/films/{id}/edit")
    public String editFilm(Model model, @PathVariable Long id){
        Film film = filmService.retrieveFilmById(id).orElseThrow(RuntimeException::new);//to catch optional return value
        List<Actor> actors = new ArrayList<>();

        actors = actorService.retrieveAllActors();

        model.addAttribute("film",film);
        model.addAttribute("actors",actors);

        return "films/film-new";
    }

    @GetMapping("/films/css-bootstrap")
    public String getFilmById(Model model) {

        model.addAttribute("films", filmService.retrieveAllFilms());

        return "films/index-css-bootstrap";
    }

}
