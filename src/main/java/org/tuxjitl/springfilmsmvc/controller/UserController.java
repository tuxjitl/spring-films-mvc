package org.tuxjitl.springfilmsmvc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.tuxjitl.springfilmsmvc.model.User;
import org.tuxjitl.springfilmsmvc.model.shop.Order;
import org.tuxjitl.springfilmsmvc.services.UserService;
import org.tuxjitl.springfilmsmvc.services.shop.OrderService;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final OrderService orderService;

    @GetMapping("/login")
    public String login() {

        return "registration/login";
    }


    @GetMapping("/register")
    public String registerView(Model model) {

        model.addAttribute("user", new User());
        return "registration/register";
    }


    @GetMapping("/my-profile")
    public String myProfileView(Model model, Principal principal){
        User loggedInUser = userService.retrieveByUsername(principal.getName());
        model.addAttribute("user", loggedInUser);
        return "users/my-profile";
    }

    @GetMapping("/my-profile/edit")
    public String editProfileView(Model model, Principal principal){
        User loggedInUser = userService.retrieveByUsername(principal.getName());
        loggedInUser.setPassword("");
        model.addAttribute("user", loggedInUser);
        return "users/profile-form";
    }

    @PostMapping("/my-profile/edit")
    public String updateProfile(@Valid User user, BindingResult br, Principal principal){
        if (br.hasErrors()){
            return "users/profile-form";
        }
        User loggedInUser = userService.retrieveByUsername(principal.getName());
        boolean allowedToUpdate = loggedInUser.getId().equals(user.getId());
        if (allowedToUpdate){
            userService.updateUser(user);
        }
        return "redirect:/my-profile?updated";
    }


    @PostMapping("/register")
    public String registerPost(@ModelAttribute("user") @Valid User
                                       newUser, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "registration/register";
        }
        userService.register(newUser);
        return "redirect:/login";
    }

    @GetMapping("/orders")
    public String ordersView(Model model, Principal principal){
        String loggedInUsername = principal.getName();
        List<Order> usersOrders = orderService.retrieveOrdersByUsername(loggedInUsername);
        usersOrders.sort(Comparator.comparing(Order::getOrderDate).reversed());
        model.addAttribute("orders", usersOrders);
        return "shop/orders";
    }

    @GetMapping("/order/{id}")
    public String orderDetailView(Model model, @PathVariable Long id, Principal principal){
        String loggedInUsername = principal.getName();
        try {
            Order foundOrder = orderService.retrieveOrderByIdAndUsername(id, loggedInUsername);
            model.addAttribute("order", foundOrder);
            return "/shop/orderDetail";
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            return "redirect:/shop/orders";
        }
    }


}
