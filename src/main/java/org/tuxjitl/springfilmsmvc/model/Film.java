package org.tuxjitl.springfilmsmvc.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Setter
@Getter
@Table(name = "film")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "title")
    @NotBlank
    private String title;
    @Column(name = "url_poster")
    private String urlPoster;
    @Column(name = "release_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past
    private LocalDate releaseDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "film_actor",joinColumns = @JoinColumn(name = "film_id"),
        inverseJoinColumns = @JoinColumn(name = "actor_id"))
    private Set<Actor> actors = new HashSet<>();

    public Film() {

    }

    public Film(String title) {

        this.title = title;
    }

    public void addActor(Actor actor){
        actors.add(actor);
    }

    @Override
    public String toString() {

        final StringBuffer sb = new StringBuffer("Film{");
        sb.append("title='").append(title).append('\'');
        sb.append(", urlPoster='").append(urlPoster).append('\'');
        sb.append(", releaseDate=").append(releaseDate);
        sb.append('}');
        return sb.toString();
    }
}
