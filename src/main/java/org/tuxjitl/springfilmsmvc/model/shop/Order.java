package org.tuxjitl.springfilmsmvc.model.shop;

import lombok.Getter;
import lombok.Setter;
import org.tuxjitl.springfilmsmvc.model.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer_order")
@Getter
@Setter
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<OrderItem> orderItems = new ArrayList<>();
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
    private LocalDateTime orderDate;
    private BigDecimal totalCost;

    public void addOrderItem(OrderItem item){
        if (! orderItems.contains(item)){
            orderItems.add(item);
        }
    }
}
