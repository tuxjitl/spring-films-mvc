package org.tuxjitl.springfilmsmvc.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Setter
@Getter
@AllArgsConstructor

public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "poster_url")
    private String urlPoster;

    @ManyToMany(mappedBy = "actors")
    private Set<Film> films = new HashSet<>();

    public Actor() {

    }

    public Actor(String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;
    }

    public Actor(long id, String firstName, String lastName,String urlPoster) {

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.urlPoster = urlPoster;

    }

    public void addFilm(Film film){
        this.films.add(film);
    }
}
